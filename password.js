const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25
}

const checkAlphabet = function (password) {
  // const alphabets = 'abcdefghigklmnopqrstuvwxyz'
  // for (const ch of password) {
  //   if (alphabets.includes(ch.toLowerCase())) return true
  // }
  // return false
  return /[a-zA-z]/.test(password)
}

const checkDigit = function (password) {
  const Digit = '123456789'
  for (const dg of password) {
    if (Digit.includes(dg)) return password.length >= 8 && password.length <= 25
  }
  return false
}

const checkSymbol = function (password) {
  const symbol = '!"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (symbol.includes(ch.toLowerCase())) return true
  }
  return false
}

const checkpassword = function (password) {
  return checkLength(password) &&
  checkAlphabet(password) &&
  checkDigit(password) &&
  checkSymbol(password)
}
module.exports = {
  checkLength,
  checkAlphabet,
  checkDigit,
  checkSymbol,
  checkpassword
}
