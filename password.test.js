const { checkLength, checkAlphabet, checkSymbol, checkpassword, checkDigit } = require('./password')
describe('Test Password Lenght', () => {
  test('should 8 character to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })

  test('should 7 charecter to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 charecter to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 25 charecter to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabet ', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet Z in password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('111')).toBe(false)
  })
  test('should has not alphabet symbol in password', () => {
    expect(checkAlphabet('!')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has ditgit to be true', () => {
    expect(checkDigit('11111Bb#')).toBe(true)
  })
  test('should has not ditgit  in password', () => {
    expect(checkDigit('abcdefgh')).toBe(false)
  })
  test('should has not ditgit M in password', () => {
    expect(checkDigit('abcdefg.')).toBe(false)
  })
  test('should 7 charecter to be false', () => {
    expect(checkDigit('1234Bb#')).toBe(false)
  })
})

describe('Test Symbol', () => {
  test('should has symbol in password to be true', () => {
    expect(checkSymbol('11111Bb#')).toBe(true)
  })
  test('should has number in password to be false', () => {
    expect(checkSymbol('11111111')).toBe(false)
  })
  test('should has not have  symbol in password to be false', () => {
    expect(checkSymbol('111111Aa')).toBe(false)
  })
})

describe('Test Password', () => {
  test('should password abc1Bb# to be falsee', () => {
    expect(checkpassword('abc1Bb#')).toBe(false)
  })
  test('should password 12345Bb# to be true', () => {
    expect(checkpassword('12345Bb!')).toBe(true)
  })
})
